package ictgradschool.web.lab10.exercise05;

public class Pojo {
    private String fName;
    private String lName;
    private String city;
    private String country;

    public Pojo(String fName, String lName, String city, String country){

        this.fName = fName;
        this.lName = lName;
        this.city = city;
        this.country = country;
    }

    public String getFName(){
        return this.fName;
    }
    public String getlName(){
        return this.lName;
    }
    public String getCity(){
        return this.city;
    }
    public String getCountry(){
        return this.country;
    }
    public void setFName(String fName){
        this.fName =fName;
    }
    public void setlName(String lName){
        this.lName =lName;
    }
    public void setCity(String city){
        this.city = city;
    }
    public void setCountry(String country){
        this.country = country;
    }

}
