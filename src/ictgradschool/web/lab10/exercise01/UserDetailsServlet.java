package ictgradschool.web.lab10.exercise01;

import ictgradschool.web.lab10.exercise05.Pojo;
import ictgradschool.web.lab10.utilities.HtmlHelper;

import javax.servlet.ServletException;
import javax.servlet.http.*;
import java.io.IOException;
import java.io.PrintWriter;
import java.net.URLDecoder;
import java.net.URLEncoder;

public class UserDetailsServlet extends HttpServlet {
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        response.setContentType("text/html");

        PrintWriter out = response.getWriter();

        // Header stuff
        out.println(HtmlHelper.getHtmlPagePreamble("Web Lab 10 - Sessions"));

        out.println("<a href=\"index.html\">HOME</a><br>");

        HttpSession firstSession =request.getSession();

        out.println("<h3>Data entered: </h3>");
        String attrFname = request.getParameter("attrFname");
        String attrLname = request.getParameter("attrLname");
        String attrCity = request.getParameter("attrCity");
        String attrCountry = request.getParameter("attrCountry");
        Pojo pojo = new Pojo(attrFname, attrLname, attrCity, attrCountry);

        firstSession.setAttribute("pojo", pojo);


        response.addCookie(new Cookie("fName", attrFname));
        response.addCookie(new Cookie("lName", attrLname));
        response.addCookie(new Cookie("city", attrCity));
        response.addCookie(new Cookie("country", URLEncoder.encode(attrCountry, "UTF-8")));
        //TODO - add the firstName, lastName, city and country  that were entered into the form to the list below
        //TODO - add the parameters from the form to session attributes
        out.println("<ul>");
        out.println("<li>First Name: </li>"+attrFname);
        out.println("<li>Last Name: </li>"+attrLname);
        out.println("<li>Country: </li>"+attrCountry);
        out.println("<li>City: </li>"+attrCity);
        out.println("</ul>");

        out.println(HtmlHelper.getHtmlPageFooter());
    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        // Process POST requests the same as GET requests
        doGet(request, response);
    }
}
