package ictgradschool.web.lab10.exercise04;

import javax.servlet.ServletException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class HitCounterServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        if ("true".equalsIgnoreCase(req.getParameter("removeCookie"))) {
            Cookie[] cookies = req.getCookies();
            if (cookies != null) {
                for (Cookie hits : req.getCookies()) {
                    if (!hits.getName().equals("0")) {
                        hits.setMaxAge(0);
                    }
                    resp.addCookie(hits);
                }
                //TODO - add code here to delete the 'hits' cookie

            }

            } else {
                boolean found = false;

                Cookie[] cookies = req.getCookies();

                if(cookies != null){
                    for (Cookie cookie : cookies) {

                        if (cookie.getName().equals("hits")) {
                            found = true;
                            int count = Integer.parseInt(cookie.getValue());
                            count++;
                            cookie.setValue(count + "");
                            resp.addCookie(cookie);
                        }

                    }

                }

                if (!found){
                    resp.addCookie(new Cookie("hits", "1"));
                }


                //TODO - add code here to get the value stored in the 'hits' cookie then increase it by 1 and update the cookie

            }

        resp.sendRedirect("hit-counter.html");
        }
        //TODO - use the response object's send redirect method to refresh the page


}


